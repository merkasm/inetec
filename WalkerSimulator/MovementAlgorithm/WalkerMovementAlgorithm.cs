﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WalkerSimulator.Components.SingleTube.Model;
using WalkerSimulator.Components.TubeSheet.Model;
using WalkerSimulator.Components.Walker.Model;

namespace WalkerSimulator.MovementAlgorithm
{
    internal class WalkerMovementAlgorithm
    {
        private TubesheetModel m_tubesheet;

        public WalkerMovementAlgorithm(TubesheetModel tm)
        {
            this.m_tubesheet = tm;
        }

        public List<MovementStep> CalculateSteps(WalkerState walkerState, TubeModel targetTube)
        {
            List<MovementStep> result = new List<MovementStep>();

            if (IsTubeReachableFromWalkerPosition(walkerState, targetTube))
            {
                //get transformation to reach tube
            }
            else
            {
                //calculate steps to reach tube
            }

            return result;
        }

        private bool IsTubeReachableFromWalkerPosition(WalkerState walkerState, TubeModel targetTube)
        {
            bool result = false;

            //pretpostavka da je lock-ana sekundarna os
            if (walkerState.LockedAxis == Helpers.AxisType.Secondary)
            {
                if (walkerState.Pincer1_Tube.Row == walkerState.Pincer2_Tube.Row)
                {
                    //sekundarna os je horizontalno postavljena
                 
                    Tuple<int, int> pointOnWalker = null;
                    Tuple<int, int> targetProjectionPont = null;

                    if (targetTube.Row < walkerState.Pincer1_Tube.Row)
                    {
                        pointOnWalker = new Tuple<int, int>(walkerState.Pincer1_Tube.Column + 1, walkerState.Pincer1_Tube.Row);
                        targetProjectionPont = new Tuple<int, int>(targetTube.Column, walkerState.Pincer1_Tube.Row);

                        var tubeOnWalker = m_tubesheet.Tubes.FirstOrDefault(t => t.Column == pointOnWalker.Item1 && t.Row == pointOnWalker.Item2);
                        var tubeFromProjection = m_tubesheet.Tubes.FirstOrDefault(t => t.Column == targetProjectionPont.Item1 && t.Row == targetProjectionPont.Item2);

                        if (tubeOnWalker != null && tubeFromProjection != null)
                        {
                            var vectorToProjection = GetVectorBetweenTubes(tubeOnWalker, tubeFromProjection);
                            var vectorToTarget = GetVectorBetweenTubes(tubeOnWalker, targetTube);
                        }
                        else
                        {
                            //error gettting tubes
                        }

                    }
                    else if (targetTube.Row > walkerState.Pincer2_Tube.Row)
                    {
                        pointOnWalker = new Tuple<int, int>(walkerState.Pincer2_Tube.Column - 1, walkerState.Pincer1_Tube.Row);
                        targetProjectionPont = new Tuple<int, int>(targetTube.Column, walkerState.Pincer1_Tube.Row);
                    }
                    else
                    {
                        pointOnWalker = new Tuple<int, int>(targetTube.Column, walkerState.Pincer1_Tube.Row);
                        if (targetTube.Row > walkerState.Pincer1_Tube.Row)
                        {
                            //točka je iznad sekundarne osi
                        }
                        else if (targetTube.Row < walkerState.Pincer1_Tube.Row)
                        {
                            //točka je ispod sekundarne osi
                        }
                        else
                        {
                            //točka je na sekundarnoj osi
                        }
                    }
                }
                else
                {
                    //sekundarna os je vertikalno postavljena
                }
            }

            //GetUpperLeftCornerOfToolSpaceRectangle();


            return result;
        }

        public void InfoDistanceAndAngleBetweenTubes(TubeModel fromTube, TubeModel toTube)
        {
            var v = GetVectorBetweenTubes(fromTube,  toTube);
        }

        private Vector GetVectorBetweenTubes(TubeModel fromTube,  TubeModel toTube)
        {
            return toTube.GlobalPosition - fromTube.GlobalPosition;
        }

        private double GetAngleBetweenTubes(Vector vectorToTube, Vector vectorFromWalker)
        {
            return Vector.AngleBetween(vectorToTube, vectorFromWalker);
        }



       
    }
}
