﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalkerSimulator.Helpers;

namespace WalkerSimulator.MovementAlgorithm
{
    public class MovementStep
    {
        public int StepID { get; set; }
        public MovementType MoveType { get; set; }
        public AxisType Axis { get; set; }
        public AxisAction Action { get; set; }
        public double TransaltionValue { get; set; }
        public double RotationValue { get; set; }
    }
}
