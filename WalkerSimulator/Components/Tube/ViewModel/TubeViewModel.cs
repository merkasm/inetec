﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using WalkerSimulator.Components.SingleTube.Model;
using WalkerSimulator.Helpers;

namespace WalkerSimulator.Components.SingleTube.ViewModel
{
    public class TubeViewModel:INotifyPropertyChanged
    {
        private bool m_isSelected = false;

        public Brush TubeStateBrush { get {  return m_TubeStateBrush; }  set { m_TubeStateBrush = value; NotifyPropertyChanged("TubeStateBrush"); }
        }
        public Brush SelectionBrush { get { return GetBrushFromSelection(); } }
               
        public TubeModel TubeInfo = null;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void SetSelection(bool selected)
        {
            m_isSelected = selected;

        }

        private Brush m_TubeStateBrush = Brushes.Gray;

        private void GetBrushFromTubeStatus()
        {

            if (TubeInfo == null)
            {
                m_TubeStateBrush = Brushes.Gray;
            }
            else
            {
                switch (TubeInfo.Status)
                {
                    case (TubeState.Ok): m_TubeStateBrush = Brushes.Green; break;
                    case (TubeState.Plugged): m_TubeStateBrush = Brushes.Black; break;
                    case (TubeState.Critical): m_TubeStateBrush = Brushes.Red; break;
                    default: m_TubeStateBrush = Brushes.Gray; break;
                }
            }

        }
        private Brush GetBrushFromSelection()
        {
            if (m_isSelected)
                return Brushes.Black;
            else return Brushes.Transparent;
        }



        public void Init(TubeModel tube)
        {
            TubeInfo = tube;
            GetBrushFromTubeStatus();
            NotifyPropertyChanged("TubeStateBrush");
        }

        public void SetTubeStatus(TubeState s)
        {
            TubeInfo.Status = s;
            GetBrushFromTubeStatus();
            NotifyPropertyChanged("TubeStateBrush");
        }
    }
}
