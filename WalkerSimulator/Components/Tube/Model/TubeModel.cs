﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WalkerSimulator.Helpers;

namespace WalkerSimulator.Components.SingleTube.Model
{
    public class TubeModel
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public TubeState Status { get; set; }
        public double Diameter_mm { get; set; }
        public double Padding_mm { get; set; }

        public Point GlobalPosition { get; set; }
    }
}
