﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WalkerSimulator.Components.SingleTube.Model;
using WalkerSimulator.Components.SingleTube.ViewModel;
using WalkerSimulator.Helpers;

namespace WalkerSimulator.Components.SingleTube.View
{
    /// <summary>
    /// Interaction logic for Tube.xaml
    /// </summary>
    public partial class Tube : UserControl
    {
        public event TubeSelected OnTubeSelected;

        public TubeModel TubeInfo { get {return ((TubeViewModel)this.DataContext).TubeInfo; }  }

        public void SetSelection(bool selected)
        {
            ((TubeViewModel)this.DataContext).SetSelection(selected);

            ShowTubeSelectedStatus(selected);
        }

    
        public Tube()
        {
            InitializeComponent();
        }

        public void Init(TubeModel tube)
        {
            ((TubeViewModel)this.DataContext).Init(tube);

            lTubeLocation.Content = $"({tube.Row}, {tube.Column})";

            this.ToolTip = $"({tube.Row}, {tube.Column})";
            
        }

        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (OnTubeSelected != null)
            {
                var selectedTube = ((TubeViewModel)this.DataContext).TubeInfo;


                if (selectedTube != null)
                    OnTubeSelected.Invoke(selectedTube.Row, selectedTube.Column);
            }
        }

        private void ShowTubeSelectedStatus(bool selected)
        {
            if (selected)
            {
                elTube.Stroke = Brushes.Green;
                elTube.StrokeThickness = 3;
            }
            else
            {
                elTube.Stroke = Brushes.Black;
                elTube.StrokeThickness = 1;

            }
        }

    }
}
