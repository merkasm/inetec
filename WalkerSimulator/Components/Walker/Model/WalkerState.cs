﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalkerSimulator.Components.SingleTube.Model;
using WalkerSimulator.Helpers;

namespace WalkerSimulator.Components.Walker.Model
{
    public class WalkerState
    {
        public AxisType LockedAxis { get; set; }
        public TubeModel Pincer1_Tube { get; set; }
        public TubeModel Pincer2_Tube { get; set; }
        public double Transaltion_mm { get; set; }
        public double Rotation_deg { get; set; }


    }
}
