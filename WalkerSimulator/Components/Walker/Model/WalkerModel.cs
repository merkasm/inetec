﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalkerSimulator.Components.SingleTube.Model;
using WalkerSimulator.Helpers;

namespace WalkerSimulator.Components.Walker.Model
{
    public class WalkerModel
    {
        public int PrimaryAxisLength { get; set; }
        public int SecondaryAxisLength { get; set; }

        public WalkerState CurrentState { get; set; }


        WalkerState m_currentState = new WalkerState();

        public void Init(TubeModel pincer1Tube, TubeModel pincer2Tube, AxisType lockedAxis, double translation_mm, double toolOrientationAngle_deg)
        {
            m_currentState.Pincer1_Tube = pincer1Tube;
            m_currentState.Pincer2_Tube = pincer2Tube;
            m_currentState.LockedAxis = lockedAxis;
            m_currentState.Transaltion_mm = translation_mm;
            m_currentState.Rotation_deg = toolOrientationAngle_deg;
        }

       }
    }

