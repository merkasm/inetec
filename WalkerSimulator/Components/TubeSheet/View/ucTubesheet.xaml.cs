﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WalkerSimulator.Components.SingleTube.Model;
using WalkerSimulator.Components.TubeSheet.Model;
using WalkerSimulator.Components.TubeSheet.ViewModel;
using WalkerSimulator.Components.SingleTube.View;

namespace WalkerSimulator.Components.TubeSheet.View
{
    /// <summary>
    /// Interaction logic for Tubesheet.xaml
    /// </summary>
    public partial class Tubesheet : UserControl
    {
        public Tubesheet()
        {
            InitializeComponent();
        }

        internal void Init(TubesheetModel model)
        {
            ((TubesheetViewModel)this.DataContext).Init(model);

            foreach (var tModel in ((TubesheetViewModel)this.DataContext).Tubes)
            {
                if (tModel != null)
                {
                    var tube = new Tube();
                    tube.Init(tModel);
                    tube.OnTubeSelected += Tube_OnTubeSelected;
                    ugTubeSheetMatrix.Children.Add(tube);
                }
                else
                {
                    //DATA ERROR
                }
            }
        }

        private void Tube_OnTubeSelected(int row, int column)
        {
            foreach (var tubeCtrl in ugTubeSheetMatrix.Children)
            {
                if (tubeCtrl.GetType() == typeof(Tube))
                {
                    if (((Tube)tubeCtrl).TubeInfo.Column == column && ((Tube)tubeCtrl).TubeInfo.Row == row)
                    {
                        ((Tube)tubeCtrl).SetSelection(true);
                    }
                    else
                        ((Tube)tubeCtrl).SetSelection(false);
                }
            }
        }
    }
}
