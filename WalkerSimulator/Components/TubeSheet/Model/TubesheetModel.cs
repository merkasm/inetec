﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalkerSimulator.Components.SingleTube.Model;

namespace WalkerSimulator.Components.TubeSheet.Model
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class TubesheetModel
    {

        private double tubesheetDiameterField;

        private double tubesheetPitchField;

        private TubeModel[] tubesField;

        /// <remarks/>
        public double TubesheetDiameter
        {
            get
            {
                return this.tubesheetDiameterField;
            }
            set
            {
                this.tubesheetDiameterField = value;
            }
        }

        /// <remarks/>
        public double TubesheetPitch
        {
            get
            {
                return this.tubesheetPitchField;
            }
            set
            {
                this.tubesheetPitchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Tube", IsNullable = false)]
        public TubeModel[] Tubes
        {
            get
            {
                return this.tubesField;
            }
            set
            {
                this.tubesField = value;
            }
        }
    }

   

}
