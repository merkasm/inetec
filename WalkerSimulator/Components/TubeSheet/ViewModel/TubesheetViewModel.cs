﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WalkerSimulator.Components.SingleTube.Model;
using WalkerSimulator.Components.TubeSheet.Model;

namespace WalkerSimulator.Components.TubeSheet.ViewModel
{
    public class TubesheetViewModel
    {
        public ObservableCollection<TubeModel> Tubes { get; set; } = new ObservableCollection<TubeModel>();
        TubesheetModel m_model = null;


        public void Init(TubesheetModel model)
        {
            m_model = model;

            var rowsCount = model.Tubes.Select(t => t.Row).Max();
            var columnsCount = model.Tubes.Select(t => t.Column).Max();
            
            for (int row = rowsCount; row > 0; row--)
            {
                for (int column = 1; column <= columnsCount; column++)
                {
                    var tModel = model.Tubes.FirstOrDefault(t => t.Row == row && t.Column == column);

                    tModel.GlobalPosition = GetTubeGlobalPosition(tModel);

                    if (tModel != null)
                    {
                        Tubes.Add(tModel);
                    }
                    else
                    {
                        //DATA ERROR
                    }
                }
            }
        }

        private Point GetTubeGlobalPosition(TubeModel tube)
        {
            return new Point(((tube.Column - 1) * m_model.TubesheetPitch + m_model.TubesheetPitch / 2),
                ((tube.Row - 1) * m_model.TubesheetPitch + m_model.TubesheetPitch / 2));
        }
    }
}
