﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WalkerSimulator.Components.TubeSheet.Model;
using WalkerSimulator.Helpers;
using WalkerSimulator.MovementAlgorithm;

namespace WalkerSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private WalkerMovementAlgorithm m_movementAlgorithm = null;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void BtnLoadTubesheet_Click(object sender, RoutedEventArgs e)
        {
            TubesheetModel tm = null;
            string tubeSheetSpecification = String.Empty;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                tubeSheetSpecification = File.ReadAllText(openFileDialog.FileName);

            if (String.IsNullOrEmpty(tubeSheetSpecification) == false)
            {
                tm = (TubesheetModel)Serializer.DeserializeObject(tubeSheetSpecification, typeof(TubesheetModel));
            }

            if (tm != null)
            {
                ctrlTubesheet.Init(tm);

                m_movementAlgorithm = new WalkerMovementAlgorithm(tm);

            }
        }
    }
}
