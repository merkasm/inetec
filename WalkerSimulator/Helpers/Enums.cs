﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalkerSimulator.Helpers
{
    public enum TubeState
    {
        Unknown,
        Ok,
        Plugged,
        Critical
    }

    public enum MovementType
    {
        Translation,
        Rotation
    }

    public enum AxisType
    {
        Primary,
        Secondary
    }

    public enum AxisAction
    {
        Lock,
        Inspect
    }
}