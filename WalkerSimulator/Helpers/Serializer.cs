﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace WalkerSimulator.Helpers
{
    internal static class Serializer
    {

        internal static string SerializeObject(object o, Type t)
        {
            string xml = "";

            try
            {
                var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                var serializer = new XmlSerializer(t);
                var settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.OmitXmlDeclaration = true;
                //settings.Encoding = Encoding.GetEncoding("ISO-8859-1");

                using (var stream = new StringWriter())
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    serializer.Serialize(writer, o, emptyNamepsaces);
                    xml = stream.ToString();
                }
            }
            catch (Exception ex)
            {

            }
            return xml;
        }

        internal static object DeserializeObject(string s, Type t)
        {
            if (String.IsNullOrEmpty(s))
                return null;

            object result = null;

            var serializer = new XmlSerializer(t);
            try
            {
                using (TextReader reader = new StringReader(s))
                {
                    result = serializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

    }
}
